package be.stecum.services.applications.property.domain.enitities;

/** @author lucs */
public class PropertyEntity {

  private VersionEntity version;
  private EnvironmentEntity environment;

  private String name;
  private String value;

  public EnvironmentEntity getEnvironment() {
    return environment;
  }

  public void setEnvironment(EnvironmentEntity environment) {
    this.environment = environment;
  }

  public VersionEntity getVersion() {
    return version;
  }

  public void setVersion(VersionEntity version) {
    this.version = version;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
