package be.stecum.services.applications.property.web.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/applications")
public class ApplicationController {

  @GET
  @Produces("application/json")
  public ApplicationListDto getApplications() {
    return null;
  }
}
