package be.stecum.services.applications.property.web.helloworld.rest;

import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Named
@Path("/hellos")
public class HelloWorldController {

  @GET
  @Path("{id}")
  @Produces("application/json")
  public HelloDto getById(@PathParam("id") String id) {
    HelloDto dto = new HelloDto();
    dto.setName(id);
    return dto;
  }

  @GET
  @Path("{id}/worlds/{worldId}")
  @Produces("application/json")
  public WorldDto getWorldId(@PathParam("id") String id, @PathParam("worldId") String worldId) {
    WorldDto dto = new WorldDto();
    dto.setName(worldId);
    HelloDto hello = new HelloDto();
    hello.setName(id);
    dto.setHello(hello);
    return dto;
  }
}
