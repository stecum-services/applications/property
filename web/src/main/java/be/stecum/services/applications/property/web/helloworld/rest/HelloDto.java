package be.stecum.services.applications.property.web.helloworld.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "hello")
public class HelloDto {

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
