package be.stecum.services.applications.property.web.helloworld.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "world")
public class WorldDto {

  private String name;
  private HelloDto hello;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public HelloDto getHello() {
    return hello;
  }

  public void setHello(HelloDto hello) {
    this.hello = hello;
  }
}
